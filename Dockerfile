FROM nginx:alpine
RUN apk add git
RUN rm -rf /usr/share/nginx/html && git clone https://github.com/cloudacademy/static-website-example.git /usr/share/nginx/html/
#RUN cd && cp -vr static-website-example/* /usr/share/nginx/html/ 
#ADD https://github.com/cloudacademy/static-website-example.git /usr/share/nginx/html